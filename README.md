# OpenML dataset: link_9

https://www.openml.org/d/45239

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Link Bayesian Network. Sample 9.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-verylarge.html#link)

- Number of nodes: 724

- Number of arcs: 1125

- Number of parameters: 14211

- Average Markov blanket size: 4.8

- Average degree: 3.11

- Maximum in-degree: 3

**Authors**: C. S. Jensen and A. Kong.

**Please cite**: ([URL](https://pubmed.ncbi.nlm.nih.gov/10441593/)): C. S. Jensen and A. Kong. Blocking Gibbs Sampling for Linkage Analysis in Large Pedigrees with Many Loops. The American Journal of Human Genetics, 65(3):885-901, 1999. Also, Research Report R-96-2048, Department of Computer Science, Aalborg University, 1996.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45239) of an [OpenML dataset](https://www.openml.org/d/45239). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45239/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45239/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45239/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

